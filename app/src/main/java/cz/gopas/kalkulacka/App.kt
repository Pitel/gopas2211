package cz.gopas.kalkulacka

import android.app.Application
import androidx.fragment.app.FragmentManager
import com.google.android.material.color.DynamicColors

class App : Application() {

    init {
        FragmentManager.enableDebugLogging(BuildConfig.DEBUG)
    }

    override fun onCreate() {
        super.onCreate()
        DynamicColors.applyToActivitiesIfAvailable(this)
    }
}
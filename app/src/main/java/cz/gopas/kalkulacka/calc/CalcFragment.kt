package cz.gopas.kalkulacka.calc

import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import cz.gopas.kalkulacka.R
import cz.gopas.kalkulacka.databinding.FragmentCalcBinding
import cz.gopas.kalkulacka.history.HistoryViewModel
import cz.gopas.kalkulacka.history.db.HistoryEntity
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class CalcFragment : Fragment() {
    private var binding: FragmentCalcBinding? = null
    private val viewModel: CalcViewModel by viewModels()
    private val historyViewModel: HistoryViewModel by viewModels({ requireActivity() })

    init {
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentCalcBinding.inflate(inflater, container, false)
        .also {
            binding = it
        }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.run {
            calc.setOnClickListener {
                val a = aLayout.editText?.text?.toString()?.toFloatOrNull() ?: Float.NaN
                val b = bLayout.editText?.text?.toString()?.toFloatOrNull() ?: Float.NaN
                viewModel.calc(a, b, ops.checkedRadioButtonId)
            }
            share.setOnClickListener { share() }
            ans.setOnClickListener { ans() }
            history.setOnClickListener { findNavController().navigate(CalcFragmentDirections.history()) }

            viewModel.calc.onEach {
                res.text = it
                if (it != null) {
                    try {
                        historyViewModel.db.insert(HistoryEntity(it.toFloat()))
                    } catch(e: SQLiteConstraintException) {
                        Log.w(TAG, e)
                    }
                }
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        historyViewModel.selected.onEach {
            Log.d(TAG, "Selected $it")
            if (it != null) {
                binding?.aLayout?.editText?.setText("$it")
                historyViewModel.selected.value = null
            }
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.about -> {
            findNavController().navigate(CalcFragmentDirections.about())
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun share() {
        val intent = Intent(Intent.ACTION_SEND)
            .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, binding?.res?.text))
            .setType("text/plain")
        startActivity(intent)
    }

    private fun ans() {
        viewModel.ans()
        binding?.bLayout?.editText?.setText(viewModel.ans.value)
    }

    private companion object {
        private val TAG = CalcFragment::class.simpleName
    }
}
package cz.gopas.kalkulacka.calc

import android.app.Application
import android.util.Log
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import cz.gopas.kalkulacka.R
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class CalcViewModel(app: Application): AndroidViewModel(app) {
    private val prefs = PreferenceManager.getDefaultSharedPreferences(app)

    private val _ans = MutableStateFlow<String?>(null)
    val ans: StateFlow<String?> = _ans

    private val _calc = MutableStateFlow<String?>(null)
    val calc: StateFlow<String?> = _calc

    fun calc(a: Float, b: Float, @IdRes op: Int) {
        Log.d(TAG, "Calc")

//        val a = binding?.aLayout?.editText?.text?.toString()?.toFloatOrNull() ?: Float.NaN
//        val b = binding?.bLayout?.editText?.text?.toString()?.toFloatOrNull() ?: Float.NaN

        _calc.value = when (op) {
            R.id.add -> a + b
            R.id.sub -> a - b
            R.id.mul -> a * b
            R.id.div -> a / b
            else -> Float.NaN
        }
            .toString()
            .also { ans ->
                prefs.edit { putString(ANS_KEY, ans) }
            }
    }

    fun ans() {
        _ans.value = prefs.getString(ANS_KEY, "")
    }

    private companion object {
        private val TAG = CalcViewModel::class.simpleName
        private const val ANS_KEY = "ans"
    }
}
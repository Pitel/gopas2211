package cz.gopas.kalkulacka

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import cz.gopas.kalkulacka.about.AboutFragment
import cz.gopas.kalkulacka.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val navController = binding.container.getFragment<NavHostFragment>().navController
        setupActionBarWithNavController(navController, AppBarConfiguration(navController.graph))
        Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Create")
        Log.d(TAG, "${intent.dataString}")
    }

    override fun onSupportNavigateUp() =
        findNavController(R.id.container).navigateUp() || super.onSupportNavigateUp()


//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        outState.putCharSequence(RES_KEY, res.text)
//    }
//
//    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        super.onRestoreInstanceState(savedInstanceState)
//        res.text = savedInstanceState.getCharSequence(RES_KEY)
//    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Resume")
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Pause")
    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.menu, menu)
//        return super.onCreateOptionsMenu(menu)
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem) = when(item.itemId) {
//        R.id.about -> {
//            supportFragmentManager.commit {
//                replace(R.id.container, AboutFragment())
//                addToBackStack(null)
//            }
//            true
//        }
//        else -> super.onOptionsItemSelected(item)
//    }

    private companion object {
        private val TAG = MainActivity::class.simpleName
        private const val RES_KEY = "result"
    }
}